BCM = "BCM"
OUT = "OUT"
HIGH = "HIGH"
LOW = "LOW"


def setmode(mode):
    print("Set mode to: " + mode)


def setup(pin_number, direction):
    print("Set pin " + str(pin_number) + " direction to " + direction)


def output(pin_number, signal):
    print("Output signal " + signal + " on pin " + str(pin_number))


class PWM(object):
    def __init__(self, pin_number, frequency):
        self.pin_number = pin_number
        self.frequency = frequency
        print("Setup PWM for pin {} with frequency {}".format(pin_number, frequency))

    def start(self, duty_cycle):
        print("Starting PWM for pin {} with duty cycle".format(self.pin_number, duty_cycle))

    def stop(self):
        print("Stop PWM for pin {}").format(self.pin_number)

    def ChangeDutyCycle(self, duty_cycle):
        print("Changing duty cycle for pin {} to {}".format(self.pin_number, duty_cycle))
